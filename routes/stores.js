const express = require('express');
const router = express.Router();
const stores = require('../controllers/stores');

router.get('/', stores.getAllStores); 
router.post('/', stores.createStore); 
router.get('/:id', stores.getStoreById); 
router.delete('/:id', stores.deleteStoreById);
router.put('/:uid', stores.updateStoreById);

module.exports = (app, nextMain) => {
    app.use('/v1/api/stores', router);
    return nextMain();
};