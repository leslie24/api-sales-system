const express = require('express');
const router = express.Router();
const products = require('../controllers/products');

router.get('/unassigned', products.getAllUnassignedProducts); 
router.get('/', products.getAllProducts); 
router.post('/', products.createProduct);
router.get('/:id', products.getProductById); 
router.delete('/:id', products.deleteProductById); 
router.put('/:uid', products.updateProductById); 

module.exports = (app, nextMain) => {
    app.use('/v1/api/products', router);
    return nextMain();
};