const express = require('express');
const router = express.Router();
const productsStores = require('../controllers/productsStores');

router.get('/asig', productsStores.getAllProductsStores); 
router.put('/transfers', productsStores.tranfersProducts); 

module.exports = (app, nextMain) => {
    app.use('/v1/api', router);
    return nextMain();
};