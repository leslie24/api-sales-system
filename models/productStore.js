module.exports = (sequelize, DataTypes) => {
    const ProductStore = sequelize.define('ProductsStores', {
        productId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Products',
                key: 'id',
            },
        },
        storeId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Stores',
                key: 'id',
            },
        },
        quantity: { 
            type: DataTypes.INTEGER,
            allowNull: false
        },

    }, {
        sequelize,
        modelName: 'ProductsStores',
    }, {
        getterMethods: {
            getId() {
                return this.id;
            }
        },
    }
    
    );

    // ProductStore.sync({force: true});
    return ProductStore;
};