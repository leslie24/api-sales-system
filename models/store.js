module.exports = (sequelize, DataTypes) => {
    const Store = sequelize.define('Store', {
        code: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        store: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {
        sequelize,
        modelName: 'Store',
    }, /*
    {
        classMethods: {
            associate: (models) => {
                Store.belongsToMany(models.Product, {
                    through: models.ProductsStores,
                    foreignKey: 'storeId',
                    as: 'products'
                });
            }
        }
    }*/
       
    );
    
    Store.associate = models => {
        Store.belongsToMany(models.Product, { through: 'ProductsStores', foreignKey: 'storeId'});
    };

    
    return Store;
};