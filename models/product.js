
module.exports = (sequelize, DataTypes) => { 
    const Product = sequelize.define('Product', {
        code: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        product: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        total: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        stock: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }   
    }, 
    {
        sequelize,
        modelName: 'Product',        
    }, /* {
        classMethods: {
            associate: (models) => {
                Product.belongsToMany(models.Store, {
                    through: models.ProductsStores,
                    foreignKey: 'productId',                   
                });
            }
        }
    },*/
    
    
    ); 

    Product.associate = models => {
        Product.belongsToMany(models.Store, { through: 'ProductsStores', foreignKey: 'productId', onDelete: 'cascade'});
    };
    
    return Product;
};


