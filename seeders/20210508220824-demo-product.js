module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.bulkInsert('Products', [
            {
                code: 'PRO001',
                product: 'PRODUCT 001',
                total: 100,
                stock: 30,
                createdAt: new Date(),
                updatedAt: new Date(),

            },
            {
                code: 'PRO002',
                product: 'PRODUCT 002',
                total: 200,
                stock: 150,
                createdAt: new Date(),
                updatedAt: new Date(),

            },
            {
                code: 'PRO003',
                product: 'PRODUCT 003',
                total: 300,
                stock: 300,
                createdAt: new Date(),
                updatedAt: new Date(),

            },
            {
                code: 'PRO004',
                product: 'PRODUCT 004',
                total: 400,
                stock: 300,
                createdAt: new Date(),
                updatedAt: new Date(),

            },
            {
                code: 'PRO005',
                product: 'PRODUCT 005',
                total: 500,
                stock: 500,
                createdAt: new Date(),
                updatedAt: new Date(),

            },
        ], {});
    },

    down: async(queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Products', null, {});
    }
};
