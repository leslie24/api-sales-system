module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.bulkInsert('ProductsStores', [
            {
                quantity: 50,
                productId: 1,
                storeId: 1,                
                createdAt: new Date(),
                updatedAt: new Date(),

            },
            {
                quantity: 20,
                productId: 1,
                storeId: 2,                
                createdAt: new Date(),
                updatedAt: new Date(),

            },
            {
                quantity: 30,
                productId: 2,
                storeId: 2,                
                createdAt: new Date(),
                updatedAt: new Date(),

            },
            {
                quantity: 20,
                productId: 2,
                storeId: 3,                
                createdAt: new Date(),
                updatedAt: new Date(),

            },
            {
                quantity: 60,
                productId: 4,
                storeId: 3,                
                createdAt: new Date(),
                updatedAt: new Date(),

            },
            {
                quantity: 40,
                productId: 4,
                storeId: 5,                
                createdAt: new Date(),
                updatedAt: new Date(),

            },

            
        ], {});
    },

    down: async(queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('ProductsStores', null, {});
    }
};
