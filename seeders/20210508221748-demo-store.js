module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.bulkInsert('Stores', [
            {
                code: 'STO001',
                store: 'STORE 001',
                createdAt: new Date(),
                updatedAt: new Date(),

            }, 
            {
                code: 'STO002',
                store: 'STORE 002',
                createdAt: new Date(),
                updatedAt: new Date(),

            }, 
            {
                code: 'STO003',
                store: 'STORE 003',
                createdAt: new Date(),
                updatedAt: new Date(),

            }, 
            {
                code: 'STO004',
                store: 'STORE 004',
                createdAt: new Date(),
                updatedAt: new Date(),

            }, 
            {
                code: 'STO005',
                store: 'STORE 005',
                createdAt: new Date(),
                updatedAt: new Date(),

            },            
        ], {});
    },

    down: async(queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Stores', null, {});
    }
};
