require('dotenv').config();
module.exports = {
    db: {
        database: process.env.DB_NAME || 'ventas',
        username: process.env.DB_USER || 'postgres',
        password: process.env.DB_PASS || 'les24code',
        host: process.env.DB_HOST || 'localhost',
        dialect: 'postgres',
        port: process.env.DB_PORT || 5432,
   
    },
    port: process.argv[2] || process.env.PORT || 8000,
};


