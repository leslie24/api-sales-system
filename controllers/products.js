
const models = require('../models');
const Product = models.Product;
const Store = models.Store;
const ProductStore = models.ProductsStores;

const { sequelize } = require('../services/database');

const Sequelize = require('sequelize');

module.exports = {
    getAllUnassignedProducts: async(req, resp, next) => {
        try {
            const products = await Product.findAll(
                {
                    attributes: ['id', 'code', 'product', 'total', 'stock'],
                    where: {
                        id: {
                            [Sequelize.Op.notIn]: sequelize.literal(`(SELECT DISTINCT "productId" FROM public."ProductsStores")`)
                        }
                    },
                    order: [['id', 'ASC']]
                });
            resp.send(products);
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message });
        }
    },
    getAllProducts: async(req, resp, next) => {
        try {
            const products = await Product.findAll({ attributes: ['id', 'code', 'product', 'total', 'stock'], order: [['id', 'ASC']]});
            resp.send(products);
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message });
        }
    },
    getProductById: async(req, resp, next) => {
        const { id } = req.params;
        try {
            const product = await Product.findOne({
                where: { id: id },
                attributes: ['id', 'code', 'product', 'total', 'stock'],
                include: [{
                    model: Store,
                    attributes: ['id', 'code', 'store'],
                    through: { attributes: ['quantity'] }
                },
                ]
            });
            if (product) {
                const result = JSON.parse(JSON.stringify(product));
                
                const stores = result.Stores.map(ele => {
                    return {
                        storeId: ele.id,
                        code: ele.code,
                        store: ele.store,
                        quantity: ele.ProductsStores.quantity
                    };
                });
                result.Stores = stores;
                resp.send(result);                
            } else {
                next(404);
            }
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message });
        }
    },
    deleteProductById: async(req, resp, next) => {
        try {
            const { id } = req.params;
            const result = await Product.destroy({
                where: {
                    id: id
                }
            });
            if (result === 1) {
                resp.send({ message: 'product was successfully removed' });
            } else {
                next(404);
            }
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message });
        }
    },
    // FALTA VALIDAR REQ.BODY
    createProduct: async(req, resp, next) => {
        try {
            let result;
            const t = await sequelize.transaction();
            const { code, product, total, stores } = req.body;
            const newProduct = await Product.create({ code, product, total, stock: total });
            result = JSON.parse(JSON.stringify(newProduct));
  
            if (stores && stores.length > 0) {
                const newStores = stores.map(ele => {
                    return { productId: newProduct.id, storeId: ele.storeId, quantity: ele.quantity };
                });
                try {
                    const newproductStores = await ProductStore.bulkCreate(newStores, { returning: true, transaction: t });
                    await t.commit();
                    result = JSON.parse(JSON.stringify(await Product.findByPk(newProduct.id)));
                    result.stores = JSON.parse(JSON.stringify(newproductStores));
                } catch (error) {
                    // console.log(error);
                    await t.rollback();
                    return resp.status(500).send({ statusCode: 500, message: error.message });
                }
            }
            resp.status(201).send(result);
        } catch (err) {
            if (err.name === 'SequelizeValidationError') {
                return resp.status(400).send({ statusCode: 400, message: err.message });
            } else {
                return resp.status(500).send({ statusCode: 500, message: err.message });
            }
        }
    },
    updateProductById: async(req, resp, next) => {
        try {
            const { uid } = req.params;            
            const { code, product, total, stores } = req.body;
            await Product.update({ code, product, total }, { returning: true, where: { id: uid}});

            if (stores && Object.keys(stores).length > 0) {
                if (stores.deleteAssignments && Object.keys(stores.deleteAssignments).length > 0) {
                    stores.deleteAssignments.forEach(async(ele) => {
                        await ProductStore.destroy({ where: { id: ele.recordId } });
                    });
                }
                if (stores.updateAssignments && Object.keys(stores.updateAssignments).length > 0) {
                    stores.updateAssignments.forEach(async(ele) => {
                        await ProductStore.update({ quantity: ele.quantity }, { returning: true, where: { id: ele.recordId }});
                    });
                }
                if (stores.newAssignments && Object.keys(stores.newAssignments).length > 0) {
                    stores.newAssignments.map(ele => {
                        ele.productId = uid;
                        return ele;
                    });
                    await ProductStore.bulkCreate(stores.newAssignments, { returning: true});
                }                
            }
            resp.send({message: 'Product updated successfully'});             
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message });
        }
    }
};