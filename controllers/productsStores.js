const models = require('../models');
const ProductStore = models.ProductsStores;

module.exports = {
    getAllProductsStores: async(req, resp, next) => {
        try {
            const productsStores = await ProductStore.findAll(); 
            resp.send(productsStores);
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message }); 
        }        
    },
    tranfersProducts: async(req, resp, next) => {
        try {
            const { origin, destiny } = req.body;
            // update origin
            const assignmentsOrigin = origin.assignments.map(ele => {
                ele.storeId = origin.storeId;
                return ele;
            });

            assignmentsOrigin .forEach(async(ele) => {
                await ProductStore.update({ quantity: ele.quantity }, { returning: true, where: { id: ele.recordId}});
            });

            if (destiny && Object.keys(destiny).length > 0) {
                // update destiny
                // news
                const newAssignmentsDestiny = destiny.assignments.newProducts.map(ele => { 
                    ele.storeId = destiny.storeId;
                    return ele;
                });
                await ProductStore.bulkCreate(newAssignmentsDestiny, { returning: true });

                // update
                const assignmentsDestiny = destiny.assignments.updateProducts.map(ele => { 
                    ele.storeId = destiny.storeId;
                    return ele;
                });

                assignmentsDestiny .forEach(async(ele) => {
                    await ProductStore.update({ quantity: ele.quantity }, { returning: true, where: { id: ele.recordId}});
                });
            }    
            resp.send({message: 'Successful products transfer'});
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message });
        }
    }
};