
const models = require('../models');
const Store = models.Store;
const Product = models.Product;
const ProductStore = models.ProductsStores;
const { sequelize } = require('../services/database');

module.exports = {
    getAllStores: async(req, resp, next) => {
        try {
            const stores = await Store.findAll({ attributes: ['id', 'code', 'store'], order: [['id', 'ASC']] }); 
            resp.send(stores);
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message }); 
        }
    },
    getStoreById: async(req, resp, next) => {
        const { id } = req.params;
        try {
            const store = await Store.findOne({
                where: { id: id },
                attributes: ['id', 'code', 'store'],
                include: [{
                    model: Product,  
                    attributes: ['id', 'code', 'product'],
                    through: { attributes: ['quantity'] }                                                    
                },
                ]              
            }); 
           
            if (store) {
                const result = JSON.parse(JSON.stringify(store));
                const products = result.Products.map(ele => {
                    return {
                        id: ele.id,
                        code: ele.code,
                        product: ele.product,
                        quantity: ele.ProductsStores.quantity
                    };
                });
                result.Products = products;
                resp.send(result);                
            } else {
                next(404);
            }
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message }); 
        }
    },
    createStore: async(req, resp, next) => {
        // validad que sea solo la lsita de productos en stock
        try {
            let result;
            const t = await sequelize.transaction();
            const { code, store, products } = req.body;   
            const newStore = await Store.create({code, store });
            result = JSON.parse(JSON.stringify(newStore));
            if (products && products.length > 0) {
                const productStores = products.map(ele => {
                    return { productId: ele.productId, storeId: newStore.id, quantity: ele.quantity };
                });
                try {
                    const newproductStores = await ProductStore.bulkCreate(productStores, { returning: true, transaction: t });
                    await t.commit();
                    result.stores = JSON.parse(JSON.stringify(newproductStores));
                } catch (error) {
                    await t.rollback();   
                    return resp.status(500).send({ statusCode: 500, message: error.message });
                }
            }
            resp.status(201).send(result); 
        } catch (err) {
            if (err.name === 'SequelizeValidationError') {
                return resp.status(400).send({ statusCode: 400, message: err.message });
            } else {
                return resp.status(500).send({ statusCode: 500, message: err.message }); 
            }     
        }         
    },
    deleteStoreById: async(req, resp, next) => {
        try {
            const { id } = req.params;
            const result = await Store.destroy({
                where: {
                    id: id
                }
            });
            if (result === 1) {
                resp.send({ message: 'store was successfully removed'});
            } else {
                next(404);
            }
        } catch (err) {
            return resp.status(500).send({ statusCode: 500, message: err.message }); 
        }                 
    },
    updateStoreById: async(req, resp, next) => {
        try {
            const { uid } = req.params;
            const { code, store, products } = req.body;
            await Store.update({ code, store }, { returning: true, where: { id: uid}});
            
            if (products && Object.keys(products).length > 0) {
                if (products.deleteAssignments && Object.keys(products.deleteAssignments).length > 0) {
                    products.deleteAssignments.forEach(async(ele) => {
                        await ProductStore.destroy({ where: { id: ele.recordId } });
                    });
                }
                if (products.updateAssignments && Object.keys(products.updateAssignments).length > 0) {
                    products.updateAssignments.forEach(async(ele) => {
                        await ProductStore.update({ quantity: ele.quantity }, { returning: true, where: { id: ele.recordId }});
                    });
                }
                if (products.newAssignments && Object.keys(products.newAssignments).length > 0) {
                    products.newAssignments.map(ele => {
                        ele.storeId = uid;
                        return ele;
                    });
                    await ProductStore.bulkCreate(products.newAssignments, { returning: true});
                }                           
            }
            resp.send({message: 'Product updated successfully'}); 
        } catch (err) {
            // console.log(err);
            return resp.status(500).send({ statusCode: 500, message: err.message }); 
        }
    }
};