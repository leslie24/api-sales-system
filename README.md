# README #
## API REST - VENTAS
### Una vez descargado, se debe:

* Ejecutar `npm install`
* Editar la configuracion de la conexion a la BD en el archivo  `config/config.json` en la seccion de development
* Realizar la migracion con : `npx sequelize-cli db:migrate`
* Insertar data con : `npx sequelize-cli db:seed:all`
* Ejecutar los trigger del archivo `Scripts_Sql/ventas.sql` atravez de SQL SHELL
* Ejecutar `npm run dev`

