const { db } = require('../config/settings');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(db.database, db.username, db.password, {
    host: db.host,
    dialect: db.dialect,
    port: db.port,
    pool: {
        max: 9,
        min: 0,
        idle: 10000
    },
    logging: false,
});

const initialize = async() => {
    try {
        await sequelize.authenticate();        
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
};

const close = async() => {
    await sequelize.close();
};


module.exports = {
    sequelize,
    initialize,
    close
};