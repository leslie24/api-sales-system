const http = require('http');
const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const pkg = require('../package.json');
const routes = require('../routes');
const errorHandler = require('../midlewares/errors');
const config = require('../config/settings');
const { corsOptions } = require('./cors');
const { port } = config;

let httpServer;

const initialize = () => new Promise((resolve, reject) => {
    const app = express();
    httpServer = http.createServer(app);
    app.set('config', config);
    app.set('pkg', pkg);
    app.set('etag', 'strong');
    app.use(helmet());
    app.use(cors(corsOptions));
    app.use(morgan('combined'));
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    // Registrar rutas
    routes(app, (err) => {
        if (err) {
            throw err;
        }
        app.use(errorHandler);

        httpServer.listen(port)
            .on('listening', () => {
                console.log(`Web server listening on http://localhost:${port}`);
                resolve();
            })
            .on('error', err => {
                reject(err);
            });
    });
});

const close = () => new Promise((resolve, reject) => {
    httpServer.close((err) => {
        if (err) {
            reject(err);
            return;
        }
        resolve();
    });
});


module.exports = {
    initialize,
    close
};