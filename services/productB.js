'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Producto extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            this.belongsToMany(models.Store, 
                { 
                    through: models.Product_Store,
                    foreignKey: 'productId'
                });
        }
    }
    Producto.init({
        code: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        product: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        total: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        stock: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }
    }, 
    {
        sequelize,
        modelName: 'Product',
    },);
    return Producto;
};

