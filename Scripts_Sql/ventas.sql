
--1. CREATE STORE WITH ASIGNED PRODUCTS UPDATE STOCK OF PRODUCT
CREATE FUNCTION public.fn_upd_stock_ai_ps()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
	DECLARE 
		BEGIN
            UPDATE "Products" SET stock = stock-NEW.quantity WHERE id = NEW."productId";                      
            RETURN NULL;
        END;
$BODY$;


CREATE TRIGGER tr_upd_stock_ai_ps AFTER INSERT ON "ProductsStores"
FOR EACH ROW  EXECUTE PROCEDURE fn_upd_stock_ai_ps();

--2. DELETE STORE UPDATE STOCK OF PRODUCT

CREATE FUNCTION public.fn_upd_stock_ad_s()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
	DECLARE 
		BEGIN
            UPDATE "Products" SET stock = stock+OLD.quantity WHERE id = OLD."productId";                      
            RETURN NULL;
        END;
$BODY$;

CREATE TRIGGER tr_upd_stock_ad_s AFTER DELETE ON "ProductsStores"
FOR EACH ROW  EXECUTE PROCEDURE fn_upd_stock_ad_s();

--3 UPDATE PRODUCTSSTORES 

CREATE FUNCTION public.fn_upd_stock_au_ps()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
	DECLARE 
		BEGIN
			 UPDATE "Products" SET stock = stock+OLD.quantity-NEW.quantity  WHERE id = OLD."productId"; 			 
			 IF ( NEW.quantity = 0 ) THEN
			 	DELETE FROM "ProductsStores" WHERE "productId"=OLD."productId" AND "storeId"= OLD."storeId";			
			END IF; 
			
			RETURN NULL;
        END;
$BODY$;

CREATE TRIGGER tr_upd_stock_au_ps AFTER UPDATE ON "ProductsStores"
FOR EACH ROW  EXECUTE PROCEDURE fn_upd_stock_au_ps();

--4.

CREATE FUNCTION public.fn_upd_stock_au_p()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
	DECLARE 
		BEGIN
            IF (OLD.total <> NEW.total) THEN
			    UPDATE "Products" SET stock = stock+(NEW.total-OLD.total) WHERE id = OLD.id;	
            END IF;		
			RETURN NULL;
        END;
$BODY$;

CREATE TRIGGER tr_upd_stock_au_p AFTER UPDATE ON "Products"
FOR EACH ROW  EXECUTE PROCEDURE fn_upd_stock_au_p();
